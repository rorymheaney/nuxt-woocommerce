import axios from 'axios';
const util = {
    getCart (self) {
		// cart items - https://docs.cocart.xyz/#retrieve-cart-contents
        axios.get('/cart/get-cart',{
			params:{
				thumb: true
			}
		})
			.then(function (response) {
				self.$store.commit('SET_WOO_CART', response.data);
			})
			.catch(function (error) {
				// handle error
				console.log(error);
			});
	},
	getCartItemCount (self){
		// total item count - https://docs.cocart.xyz/#count-items
		axios.get('/cart/count-items')
			.then(function (response) {
				self.$store.commit('SET_WOO_CART_ITEM_COUNT', response.data);
			})
			.catch(function (error) {
				// handle error
				console.log(error);
			});
	},
   	calculateCart (self) {
        // grand total - https://docs.cocart.xyz/#calculate-cart-totals
		axios.post('/cart/calculate',{
			return: true
		})
			.then(function (response) {
				self.$store.commit('SET_WOO_CART_TOTALS', response.data);
			})
			.catch(function (error) {
				// handle error
				console.log(error);
			});
	},
	// self will serve as 'this'
	setCartPreview(self){
		this.getCart(self);
		this.getCartItemCount(self);
		this.calculateCart(self);
	},
	deleteItem(id){
		// id is actually the product key, item.key (example)
		axios.delete('/cart/item',{
			params: {
				cart_item_key: id,
			}
		})
			.then(function (response) {
				console.log(response);
			})
			.catch(function (error) {
				// handle error
				console.log(error);
			});
	}
}

export default util
