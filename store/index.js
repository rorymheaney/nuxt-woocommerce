import axios from 'axios';

export const state = () => ({
	wooCommerceCart: '',
	wooCommerceTotal: '',
	wooCommerceItemCount: ''
})

export const mutations = {
	SET_WOO_CART(state, payload){
		state.wooCommerceCart = payload
	},
	SET_WOO_CART_TOTALS(state, payload){
		state.wooCommerceTotal = payload
	},
	SET_WOO_CART_ITEM_COUNT(state, payload){
		state.wooCommerceItemCount = payload
	}
}

export const actions = {
	async nuxtServerInit({commit}) {
		// get cart on page load
		// const cartSession = await axios.get('');

		// commit('SET_WOO_CART', cartSession);
	}
}
