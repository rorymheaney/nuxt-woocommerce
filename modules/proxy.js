const env = require('dotenv').config();
const proxyApis = {
	"/load-form/": {
		target: process.env.BASE_URL + "gf/v2/forms/2",
		auth: process.env.G_FORMS_K + ':' + process.env.G_FORMS_K_SECRET,
		pathRewrite: { "^/load-form/": "" },
		changeOrigin: true
	},
	"/ecomm/": {
		target: "https://wordcamp-wp-nuxt-admin.fancysquares.blog/",
		auth: process.env.WOO_COMMERCE_K + ':' + process.env.WOO_COMMERCE_K_SECRET,
		pathRewrite: { "^/ecomm/": "/wp-json/wc/v3/" },
	},
	"/cart/": {
		target: "https://wordcamp-wp-nuxt-admin.fancysquares.blog/",
		auth: process.env.WOO_COMMERCE_K + ':' + process.env.WOO_COMMERCE_K_SECRET,
		pathRewrite: { "^/cart/": "/wp-json/cocart/v1/" },
	}
}
module.exports = proxyApis;
