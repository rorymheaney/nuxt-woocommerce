const express = require("express");
const consola = require("consola");
const { Nuxt, Builder } = require("nuxt");
const app = express();
app.disable('x-powered-by');
// Import and Set Nuxt.js options
const config = require("../nuxt.config.js");
config.dev = process.env.NODE_ENV !== "production";

async function start() {
	// Init Nuxt.js
	const nuxt = new Nuxt(config);

	const { host, port } = nuxt.options.server;

	// Build only in dev mode
	if (config.dev) {
		const builder = new Builder(nuxt);
		await builder.build();
	} else {
		await nuxt.ready();
	}

	// Give nuxt middleware to express
	app.use(nuxt.render);


	// Listen the server
	app.listen(port, host);
	consola.ready({
		message: `Server listening on http://${host}:${port}`,
		badge: true
	});
}
start();

// const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;

// const api = new WooCommerceRestApi({
// 	url: "http://headless-woocommerce.test",
// 	consumerKey: "ck_e71bb24552200983a7b52da03ac78981a0aa51d5",
// 	consumerSecret: "cs_33ae811bc9eeae9ede3206a33c5e59b222b14b46",
// 	version: "wc/v3"
// });
// const ads = [{ title: "Hello, world (again)!" }];

// app.get("/testing", (req, res) => {
// 	// res.send(ads);
// 	api.get("products", {
// 		per_page: 20, // 20 products per page
// 	}).then(function (response) {
// 		// self.product = response;
// 		console.log("Response Status:", response.status);
// 		// console.log("Response Headers:", response.headers);
// 		// console.log("Response Data:", response.data);
// 		console.log("Total of pages:", response.headers['x-wp-totalpages']);
// 		console.log("Total of items:", response.headers['x-wp-total']);
// 		res.send(product.data);
// 	})
// 	.catch(function (error) {
// 		console.log(error);
// 	});
// 	// res.send(response);

// });
